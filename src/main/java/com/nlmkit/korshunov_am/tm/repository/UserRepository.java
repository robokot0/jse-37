package com.nlmkit.korshunov_am.tm.repository;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.nlmkit.korshunov_am.tm.entity.User;
import com.nlmkit.korshunov_am.tm.enumerated.Role;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserRepository {
    /**
     * Приватный конструктор по умолчанию
     */
    private UserRepository(){
    }
    /**
     * Единственный экземпляр объекта UserRepository
     */
    private static UserRepository instance = null;

    /**
     * Получить единственный экземпляр объекта UserRepository
     * @return единственный экземпляр объекта UserRepository
     */
    public static UserRepository getInstance(){
        if (instance == null){
            instance = new UserRepository();
        }
        return instance;
    }

    private List<User> users = new ArrayList<>();

    /**
     * Создать пользователя
     * @param login имя
     * @return пользователь
     */
    public User create(final String login) {
        final User user = new User(login);
        users.add(user);
        return user;
    }
    /**
     * Создать пользователя
     * @param login login
     * @param role роль
     * @param firstName Имя
     * @param secondName Фамилия
     * @param middleName Отчество
     * @param passwordHash Хэш пароля
     * @return пользователь
     */
    public User create(final String login, final Role role, final String firstName, final String secondName, final String middleName, final String passwordHash) {
        final User user = new User();
        user.setLogin(login);
        user.setRole(role);
        user.setFirstName(firstName);
        user.setSecondName(secondName);
        user.setMiddleName(middleName);
        user.setPasswordHash(passwordHash);
        users.add(user);
        return user;
    }
    /**
     * Изменить пользователя
     * @param id идентификатор
     * @param login login
     * @param role роль
     * @param firstName Имя
     * @param secondName Фамилия
     * @param middleName Отчество
     * @param passwordHash Хэш пароля
     * @return пользователь
     */
    public Optional<User> update(final Long id,final String login, final Role role, final String firstName, final String secondName, final String middleName, final String passwordHash)  {
        final var user = findById(id);
        if (user.isEmpty()) {
            return user;
        }
        user.get().setLogin(login);
        user.get().setRole(role);
        user.get().setFirstName(firstName);
        user.get().setSecondName(secondName);
        user.get().setMiddleName(middleName);
        user.get().setPasswordHash(passwordHash);
        return user;
    }
    /**
     * Изменить данные пользователя
     * @param id идентификатор
     * @param login login
     * @param role роль
     * @param firstName Имя
     * @param secondName Фамилия
     * @param middleName Отчество
     * @return пользователь
     */
    public Optional<User> updateData(final Long id,final String login, final Role role, final String firstName, final String secondName, final String middleName) {
        final var user = findById(id);
        if (user.isEmpty()) {
            return user;
        }
        user.get().setLogin(login);
        user.get().setRole(role);
        user.get().setFirstName(firstName);
        user.get().setSecondName(secondName);
        user.get().setMiddleName(middleName);
        return user;
    }
    /**
     * Изменить пароль пользователя по идентификатору
     * @param id идентификатор
     * @param passwordHash хэш пароля
     * @return пользователь
     */
    public Optional<User> updatePasswordHash(final Long id, String passwordHash) {
        final var user = findById(id);
        if (user.isEmpty()) {
            return user;
        }
        user.get().setPasswordHash(passwordHash);
        return user;
    }
    /**
     * Удалить всех пользователей.
     */
    public void clear() {
        users.clear();
    }
    /**
     * Найти пользователя по идентификатору.
     * @param id идентификатор
     * @return Optional пользователь
     */
    public Optional<User> findById(final Long id){
        for (final var user: users) {
            if (user.getId().equals(id)) return Optional.of(user);
        }
        return Optional.empty();
    }
    /**
     * Найти пользователя по индексу.
     * @param index Индекс
     * @return пользователь
     */
    public Optional<User> findByIndex(final int index){
        return Optional.of(users.get(index));
    }
    /**
     * Найти пользователя по login.
     * @param login login
     * @return пользователь
     */
    public Optional<User> findByLogin(final String login){
        for (final var user: users) {
            if (user.getLogin().equals(login)) return Optional.of(user);
        }
        return Optional.empty();
    }
    /**
     * Удалить пользователя по идентификатору
     * @param id идентифкатор
     * @return пользователь
     */
    public Optional<User> removeById(final Long id) {
        final var user = findById(id);
        if (user.isEmpty()){
            return user;
        }
        users.remove(user.get());
        return user;
    }
    /**
     * Получить список всех пользователей
     * @return список пользователей
     */
    public List<User> findAll() {
        return users;
    }

    /**
     * Получить количество пользователей
     * @return количество пользователей
     */
    public int size() {
        return users.size();
    }
    /**
     * Сохранить в поток
     * @throws IOException ошибка ввода вывода
     */
    public void saveAs(ObjectMapper objectMapper, OutputStream outputStream) throws IOException {
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(outputStream,this.users);
    }
    /**
     * Загрузить из потока
     * @throws IOException ошибка ввода вывода
     */
    public void loadFrom(ObjectMapper objectMapper,InputStream inputStream)  throws IOException {
        clear();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        JavaType type = objectMapper.getTypeFactory().constructCollectionType(ArrayList.class, User.class) ;
        users = objectMapper.readValue(inputStream, type);
    }
}
