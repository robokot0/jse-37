package com.nlmkit.korshunov_am.tm.listener;

import com.nlmkit.korshunov_am.tm.entity.Command;
import com.nlmkit.korshunov_am.tm.service.CommandHistoryService;

import static com.nlmkit.korshunov_am.tm.constant.TerminalConst.*;

public class CommandHistoryListener extends AbstractListener implements Listener {
    /**
     * Приватный конструктор по умолчанию
     */
    public CommandHistoryListener(CommandHistoryService commandHistoryService){
        super(commandHistoryService);
    }
    /**
     * Приватный конструктор по умолчанию
     */
    private CommandHistoryListener(){
        super(CommandHistoryService.getInstance());
    }
    /**
     * Единственный экземпляр объекта CommandHistoryListener
     */
    private static CommandHistoryListener instance = null;

    /**
     * Получить единственный экземпляр объекта CommandHistoryListener
     * @return единственный экземпляр объекта CommandHistoryListener
     */
    public static CommandHistoryListener getInstance(){
        if (instance == null){
            instance = new CommandHistoryListener();
        }
        return instance;
    }

    @Override
    public int notify(String command) {
        switch (command) {
            case SHORT_COMMAND_HISTORY_VIEW:
            case COMMAND_HISTORY_VIEW: return listCommandHistory();
            case SHORT_HELP:
            case HELP:return displayHelp();
            default:return -1;
        }
    }

    /**
     * Показать историю комманд
     * @return 0 успешно показана история
     */
    public int listCommandHistory(){
        System.out.println("[LIST COMMAND HISTORY]");
        int index = 1;
        for (final Command command: commandHistoryService.findAll())
        {
            System.out.println(index + ". " + command.getCommandText());
            index ++;
        }
        ShowResult("[OK]");
        return 0;
    }
    /**
     * Показать справку
     */
    public int displayHelp() {
        logger.info("command-history-view - View command history. (chv)");
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Добавляет комманду в историю
     * @param command комманда
     */
    public void addCommandToHistory(String command) {
        commandHistoryService.addCommandToHistory(command);
    }
}
