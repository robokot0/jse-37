package com.nlmkit.korshunov_am.tm.service;

import com.nlmkit.korshunov_am.tm.entity.Task;
import com.nlmkit.korshunov_am.tm.repository.TaskRepostory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.List;

public class TaskWarningService {
    /**
     * Логгер
     */
    final Logger logger = LogManager.getLogger(this.getClass().getName());
    /**
     * Репозитарий задач
     */
    TaskRepostory taskRepostory;
    /**
     * Приватный кконструктор по умолчанию
     */
    private TaskWarningService() {
        this.taskRepostory = TaskRepostory.getInstance();
    }
    /**
     * Конструктор длоя тестирвоания
     * @param taskRepostory Репозитарий задач
     */
    public TaskWarningService(TaskRepostory taskRepostory) {
        this.taskRepostory = taskRepostory;
    }
    /**
     * Единственный экземпляр объекта TaskWarningService
     */
    private static TaskWarningService instance = null;

    /**
     * Получить единственный экземпляр объекта TaskWarningService
     * @return единственный экземпляр объекта TaskWarningService
     */
    public static TaskWarningService getInstance(){
        if (instance == null){
            instance = new TaskWarningService();
        }
        return instance;
    }

    /**
     * Список задач по которым надо показать уведомления
     * @param userId пользователь
     * @return Список задач
     */

    public List<Task> findTaskForWarning(final Long userId) {
        logger.debug("findTaskForWarning({})",userId);
        LocalDateTime now = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);
        LocalDateTime now4 = now.plusHours(4);
        LocalDateTime now1 = now.plusHours(1);
        LocalDateTime now30 = now.plusMinutes(30);
        LocalDateTime now15 = now.plusMinutes(15);
        LocalDateTime now5 = now.plusMinutes(5);
        logger.debug("findTaskForWarning:now={}",now);
        logger.debug("findTaskForWarning:now4={}",now4);
        logger.debug("findTaskForWarning:now1={}",now1);
        logger.debug("findTaskForWarning:now30={}",now30);
        logger.debug("findTaskForWarning:now15={}",now15);
        logger.debug("findTaskForWarning:now5={}",now5);
        List<Task> taskList = taskRepostory.findAllByDeadLinePeriod(now5.minusSeconds(30),now4.plusSeconds(30),userId);
        taskList.removeIf(i ->
        {
            LocalDateTime deadLine = i.getDeadLine();
            logger.debug("findTaskForWarning:removeIf:task={} ",i);
            logger.debug("findTaskForWarning:removeIf:deadLine={} ",deadLine);
            boolean result =
                    !((deadLine.compareTo(now4.minusSeconds(30)) > 0 && deadLine.compareTo(now4) <= 0)
                    || (deadLine.compareTo(now1.minusSeconds(30)) > 0 && deadLine.compareTo(now1) <= 0)
                    || (deadLine.compareTo(now30.minusSeconds(30)) > 0 && deadLine.compareTo(now30) <= 0)
                    || (deadLine.compareTo(now15.minusSeconds(30)) > 0 && deadLine.compareTo(now15) <= 0)
                    || (deadLine.compareTo(now5.minusSeconds(30)) > 0 && deadLine.compareTo(now5) <= 0)
            );
            logger.debug("findTaskForWarning:removeIf:result={} ",result);
            return result;
        }
        );
        if(taskList.size()==0){
            return taskList;
        }
        taskList.sort(Comparator.comparing(Task::getDeadLine));
        return taskList;
    }

}
