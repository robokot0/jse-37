package com.nlmkit.korshunov_am.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nlmkit.korshunov_am.tm.entity.User;
import com.nlmkit.korshunov_am.tm.enumerated.Role;
import com.nlmkit.korshunov_am.tm.repository.UserRepository;

import java.io.*;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Optional;

public class UserService extends AbstractService{
    private final java.security.MessageDigest md;
    /**
     * Приватный конструктор по умолчанию
     */
    private UserService() {
        super();
        this.userRepository = UserRepository.getInstance();
        this.md = getMd("MD5");
    }
    /**
     * конструктор для тестирования
     */
    public UserService(UserRepository userRepository,java.security.MessageDigest md){
        super();
        this.userRepository=userRepository;
        this.md=md;
    }
    /**
     * Возвращает алгоритм по названию
     * @param algoritm algoritm
     * @return MessageDigest
     */
    public static java.security.MessageDigest getMd(String algoritm) {
        java.security.MessageDigest md;
        try {
            md = java.security.MessageDigest.getInstance(algoritm);
        } catch (NoSuchAlgorithmException e) {
            md=null;
        }
        return md;
    }
    /**
     * Единственный экземпляр объекта UserService
     */
    private static UserService instance = null;
    /**
     * Получить единственный экземпляр объекта UserService
     * @return единственный экземпляр объекта UserService
     */
    public static UserService getInstance() {
        if (instance == null){
            instance = new UserService();
        }
        return instance;
    }
    /**
     * Репозитарий пользователей
     */
    private final UserRepository userRepository;

    /**
     * Вычислить хэш строки
     * @param stringtohash строка для хэширования
     * @return хэш строки
     */
    public String getStringHash(String stringtohash) {
        if (this.md==null) {
            return null;
        } else {
            final var array = md.digest(stringtohash.getBytes());
            final var sb = new StringBuilder();
            for (final var b : array) {
                sb.append(Integer.toHexString((b & 0xFF) | 0x100), 1, 3);
            }
            return sb.toString();
        }
    }
    /**
     * Создать пользователя
     * @param login имя
     * @return пользователь
     */
    public Optional<User> create(String login) {
        if (login == null || login.isEmpty()) return Optional.empty();
        if (userRepository.findByLogin(login).isPresent()) return Optional.empty();
        return Optional.ofNullable(userRepository.create(login));
    }

    /**
     * Создать пользователя
     * @param login login
     * @param role роль
     * @param firstName Имя
     * @param secondName Фамилия
     * @param middleName Отчество
     * @param passwordHash Хэш пароля
     * @return пользователь
     */
    public Optional<User> create(final String login, final Role role, final String firstName, final String secondName, final String middleName, final String passwordHash) {
        if (login == null || login.isEmpty()) return Optional.empty();
        if (userRepository.findByLogin(login).isPresent()) return Optional.empty();
        if (passwordHash == null || passwordHash.isEmpty()) return Optional.empty();
        return Optional.ofNullable(userRepository.create(login,role,firstName,secondName,middleName,passwordHash));
    }
    /**
     * Изменить пользователя
     * @param id идентификатор
     * @param login login
     * @param role роль
     * @param firstName Имя
     * @param secondName Фамилия
     * @param middleName Отчество
     * @param passwordHash Хэш пароля
     * @return пользователь
     */
    public Optional<User> update(final Long id,final String login, final Role role, final String firstName, final String secondName, final String middleName, final String passwordHash) {
        if (id == null) return Optional.empty();
        if (login == null || login.isEmpty()) return Optional.empty();
        if (passwordHash == null || passwordHash.isEmpty()) return Optional.empty();
        if (userRepository.findById(id).isEmpty()) return Optional.empty();
        final var sameLoginUser = userRepository.findByLogin(login);
        if(sameLoginUser.isPresent()){
            if(!sameLoginUser.get().getId().equals(id)){
                return Optional.empty();
            }
        }
        return userRepository.update(id,login,role,firstName,secondName,middleName,passwordHash);
    }
    /**
     * Изменить данные пользователя
     * @param id идентификатор
     * @param login login
     * @param role роль
     * @param firstName Имя
     * @param secondName Фамилия
     * @param middleName Отчество
     * @return пользователь
     */
    public Optional<User> updateData(final Long id,final String login, final Role role, final String firstName, final String secondName, final String middleName) {
        if (id == null) return Optional.empty();
        if (login == null || login.isEmpty()) return Optional.empty();
        if (userRepository.findById(id).isEmpty()) return Optional.empty();
        final var sameLoginUser = userRepository.findByLogin(login);
        if(sameLoginUser.isPresent()){
            if(!sameLoginUser.get().getId().equals(id)){
                return Optional.empty();
            }
        }
        return userRepository.updateData(id,login,role,firstName,secondName,middleName);
    }
    /**
     * Изменить пароль пользователя
     * @param id идентификатор
     * @param passwordHash Хэш пароля
     * @return пользователь
     */
    public Optional<User> updatePassword(final Long id, String passwordHash) {
        if (id == null) return Optional.empty();
        if (passwordHash == null || passwordHash.isEmpty()) return Optional.empty();
        if (userRepository.findById(id).isEmpty()) return Optional.empty();
        return userRepository.updatePasswordHash(id, passwordHash);
    }

    /**
     * Удалить всех пользователей.
     */
    public void clear() {
        userRepository.clear();
    }
    /**
     * Найти пользователя по id.
     * @param id id
     * @return пользователь
     */
    public Optional<User> findById(final Long id) {
        if (id == null) return Optional.empty();
        return userRepository.findById(id);
    }
    /**
     * Найти пользователя по login.
     * @param login login
     * @return пользователь
     */
    public Optional<User> findByLogin(String login) {
        if (login == null || login.isEmpty()) return Optional.empty();
        return userRepository.findByLogin(login);
    }
    /**
     * Найти пользователя по индексу.
     * @param index index
     * @return пользователь
     */
    public Optional<User> findByIndex(final int index) {
        return userRepository.findByIndex(index);
    }
    /**
     * Удалить пользователя по id
     * @param id id
     * @return login
     */
    public Optional<User> removeById(final Long id) {
        if (id == null) return Optional.empty();
        if (userRepository.findById(id).isEmpty()) return Optional.empty();
        return userRepository.removeById(id);
    }
    /**
     * Получить список всех пользователей
     * @return список пользователей
     */
    public List<User> findAll() {
        return userRepository.findAll();
    }

    /**
     * Сохранить в поток
     * @throws IOException ошибка ввода вывода
     * @param objectMapper objectMapper
     * @param outputStream outputStream
     */
    public void saveAs(ObjectMapper objectMapper, OutputStream outputStream) throws IOException {
        userRepository.saveAs(objectMapper, outputStream);
    }

    /**
     * Загрузить из потока
     * @throws IOException ошибка ввода вывода
     * @param objectMapper objectMapper
     * @param inputStream inputStream
     */
    public void loadFrom(ObjectMapper objectMapper, InputStream inputStream) throws IOException {
        userRepository.loadFrom(objectMapper, inputStream);
    }
}
