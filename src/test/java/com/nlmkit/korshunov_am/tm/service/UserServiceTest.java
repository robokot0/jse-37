package com.nlmkit.korshunov_am.tm.service;

import com.nlmkit.korshunov_am.tm.entity.User;
import com.nlmkit.korshunov_am.tm.enumerated.Role;
import com.nlmkit.korshunov_am.tm.repository.UserRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class UserServiceTest {
    static UserRepository userRepository;
    static UserService userService;

    @BeforeAll
    public static void doBeforeAll() {
        userRepository = Mockito.mock(UserRepository.class);
        assertDoesNotThrow(()-> {
            userService = new UserService(userRepository, UserService.getMd("MD5"));
        });
    }
    @Test
    void getMd() {
        assertNotNull(UserService.getMd("MD5"));
        assertNull(UserService.getMd("aaaaaa"));
    }

    @Test
    void getInstance() {
        UserService userService = UserService.getInstance();
        assertEquals(userService,UserService.getInstance());
    }

    @Test
    void getStringHash() {
        assertNotNull(userService.getStringHash("1111"));
    }

    @Test
    void getStringHashMdNull() {
        UserService userService = new UserService(userRepository, UserService.getMd("11111"));
        assertNull(userService.getStringHash("1111"));
    }

    @Test
    void create() {
        assertDoesNotThrow(()->{
            userService.create("login");
            Mockito.verify(userRepository,Mockito.times(1)).findByLogin("login");
            Mockito.verify(userRepository,Mockito.times(1)).create("login");
        });
        assertDoesNotThrow(()->{
            Mockito.doReturn(Optional.of(new User("login1"))).when(userRepository).findByLogin("login1");
            userService.create("login1");
            Mockito.verify(userRepository,Mockito.times(1)).findByLogin("login1");
            Mockito.verify(userRepository,Mockito.times(0)).create("login1");
        });
        assertFalse(userService.create(null).isPresent());
        assertFalse(userService.create("").isPresent());
    }

    @Test
    void testCreate() {
        assertDoesNotThrow(()->{
            userService.create("testCreate", Role.ADMIN,"fn","sn","mn","ph");
            Mockito.verify(userRepository,Mockito.times(1)).findByLogin("testCreate");
            Mockito.verify(userRepository,Mockito.times(1)).create("testCreate", Role.ADMIN,"fn","sn","mn","ph");
        });
        assertDoesNotThrow(()->{
            Mockito.doReturn(Optional.of(new User("testCreate1"))).when(userRepository).findByLogin("testCreate1");
            userService.create("testCreate1", Role.ADMIN,"fn","sn","mn","ph");
            Mockito.verify(userRepository,Mockito.times(1)).findByLogin("testCreate1");
            Mockito.verify(userRepository,Mockito.times(0)).create("testCreate1", Role.ADMIN,"fn","sn","mn","ph");
        });
        assertFalse(userService.create(null, Role.ADMIN,"fn","sn","mn","ph").isPresent());
        assertFalse(userService.create("", Role.ADMIN,"fn","sn","mn","ph").isPresent());
        assertFalse(userService.create("login", Role.ADMIN,"fn","sn","mn","").isPresent());
        assertFalse(userService.create("login", Role.ADMIN,"fn","sn","mn",null).isPresent());
    }

    @Test
    void update() {
        assertDoesNotThrow(()->{
            userService.update(null,"update", Role.ADMIN,"fn","sn","mn","ph");
        });
        assertDoesNotThrow(()->{
            userService.update(1331L,"update", Role.ADMIN,"fn","sn","mn","ph");
            Mockito.verify(userRepository,Mockito.times(1)).findById(1331L);
            Mockito.verify(userRepository,Mockito.times(0)).findByLogin("update");
            Mockito.verify(userRepository,Mockito.times(0)).update(1331L,"update", Role.ADMIN,"fn","sn","mn","ph");
        });
        assertDoesNotThrow(()->{
            User user = new User("update");
            user.setId(12L);
            User user1 = new User("update1");
            user.setId(13L);
            Mockito.doReturn(Optional.of(user)).when(userRepository).findById(12L);
            Mockito.doReturn(Optional.of(user1)).when(userRepository).findByLogin("update1");
            userService.update(12L,"update1", Role.ADMIN,"fn","sn","mn","ph");
            Mockito.verify(userRepository,Mockito.times(1)).findById(12L);
            Mockito.verify(userRepository,Mockito.times(1)).findByLogin("update1");
            Mockito.verify(userRepository,Mockito.times(0)).update(12L,"update1", Role.ADMIN,"fn","sn","mn","ph");
        });
        assertDoesNotThrow(()->{
            User user = new User("update");
            user.setId(14L);
            Mockito.doReturn(Optional.of(user)).when(userRepository).findById(14L);
            Mockito.doReturn(Optional.of(user)).when(userRepository).findByLogin("update");
            userService.update(14L,"update", Role.ADMIN,"fn","sn","mn","ph");
            Mockito.verify(userRepository,Mockito.times(1)).findById(14L);
            Mockito.verify(userRepository,Mockito.times(1)).findByLogin("update");
            Mockito.verify(userRepository,Mockito.times(1)).update(14L,"update", Role.ADMIN,"fn","sn","mn","ph");
        });
        assertDoesNotThrow(()->{
            assertFalse(userService.update(null,"login", Role.ADMIN,"fn","sn","mn","ph").isPresent());
            assertFalse(userService.update(0L,null, Role.ADMIN,"fn","sn","mn","ph").isPresent());
            assertFalse(userService.update(0L,"", Role.ADMIN,"fn","sn","mn","ph").isPresent());
            assertFalse(userService.update(0L,"login", Role.ADMIN,"fn","sn","mn","").isPresent());
            assertFalse(userService.update(0L,"login", Role.ADMIN,"fn","sn","mn",null).isPresent());
        });
    }

    @Test
    void updateData() {
        assertDoesNotThrow(()->{
            userService.updateData(null,"updateData", Role.ADMIN,"fn","sn","mn");
        });
        assertDoesNotThrow(()->{
            userService.updateData(2220L,"updateData", Role.ADMIN,"fn","sn","mn");
            Mockito.verify(userRepository,Mockito.times(1)).findById(2220L);
            Mockito.verify(userRepository,Mockito.times(0)).findByLogin("updateData");
            Mockito.verify(userRepository,Mockito.times(0)).updateData(2220L,"updateData", Role.ADMIN,"fn","sn","mn");
        });
        assertDoesNotThrow(()->{
            User user = new User("updateData");
            user.setId(22L);
            User user1 = new User("updateData1");
            user.setId(23L);
            Mockito.doReturn(Optional.of(user)).when(userRepository).findById(22L);
            Mockito.doReturn(Optional.of(user1)).when(userRepository).findByLogin("updateData1");
            userService.updateData(22L,"updateData1", Role.ADMIN,"fn","sn","mn");
            Mockito.verify(userRepository,Mockito.times(1)).findById(22L);
            Mockito.verify(userRepository,Mockito.times(1)).findByLogin("updateData1");
            Mockito.verify(userRepository,Mockito.times(0)).updateData(22L,"updateData1", Role.ADMIN,"fn","sn","mn");
        });
        assertDoesNotThrow(()->{
            User user = new User("updateData");
            user.setId(24L);
            Mockito.doReturn(Optional.of(user)).when(userRepository).findById(24L);
            Mockito.doReturn(Optional.of(user)).when(userRepository).findByLogin("updateData");
            userService.updateData(24L,"updateData", Role.ADMIN,"fn","sn","mn");
            Mockito.verify(userRepository,Mockito.times(1)).findById(24L);
            Mockito.verify(userRepository,Mockito.times(1)).findByLogin("updateData");
            Mockito.verify(userRepository,Mockito.times(1)).updateData(24L,"updateData", Role.ADMIN,"fn","sn","mn");
        });
        assertDoesNotThrow(()->{
            assertFalse(userService.updateData(null,"login", Role.ADMIN,"fn","sn","mn").isPresent());
            assertFalse(userService.updateData(0L,null, Role.ADMIN,"fn","sn","mn").isPresent());
            assertFalse(userService.updateData(0L,"", Role.ADMIN,"fn","sn","mn").isPresent());
        });
   }

    @Test
    void updatePassword() {
        assertDoesNotThrow(()->{
            userService.updatePassword(30L,"ph");
            Mockito.verify(userRepository,Mockito.times(1)).findById(30L);
            Mockito.verify(userRepository,Mockito.times(0)).updatePasswordHash(30L,"ph");
        });
        assertDoesNotThrow(()->{
            User user = new User("updatePassword");
            user.setId(34L);
            Mockito.doReturn(Optional.of(user)).when(userRepository).findById(34L);
            userService.updatePassword(34L,"ph");
            Mockito.verify(userRepository,Mockito.times(1)).findById(34L);
            Mockito.verify(userRepository,Mockito.times(1)).updatePasswordHash(34L,"ph");
        });
        assertDoesNotThrow(()->{
            assertFalse(userService.updatePassword(null,"ph").isPresent());
            assertFalse(userService.updatePassword(0L,"").isPresent());
            assertFalse(userService.updatePassword(0L,null).isPresent());
        });
   }

    @Test
    void clear() {
        userService.clear();
        verify(userRepository,Mockito.times(1)).clear();
    }

    @Test
    void findById() {
        assertDoesNotThrow(()->{
            userService.findById(50L);
            verify(userRepository,times(1)).findById(50L);
        });
        assertFalse(userService.findById(null).isPresent());
    }

    @Test
    void findByLogin() {
        assertDoesNotThrow(()->{
            userService.findByLogin("findByLogin");
            verify(userRepository,times(1)).findByLogin("findByLogin");
        });
        assertFalse(userService.findByLogin(null).isPresent());
        assertFalse(userService.findByLogin("").isPresent());
    }

    @Test
    void findByIndex() {
        assertDoesNotThrow(()->{
            userService.findByIndex(0);
            verify(userRepository,times(1)).findByIndex(0);
        });
    }

    @Test
    void removeById() {
        assertDoesNotThrow(()->{
            userService.removeById(60L);
            verify(userRepository,times(1)).findById(60L);
            verify(userRepository,times(0)).removeById(60L);
        });
        assertDoesNotThrow(()->{
            User user = new User("login");
            user.setId(61L);
            doReturn(Optional.of(user)).when(userRepository).findById(61L);
            userService.removeById(61L);
            verify(userRepository,times(1)).findById(61L);
            verify(userRepository,times(1)).removeById(61L);
        });
        assertDoesNotThrow(()->{
            assertFalse(userService.removeById(null).isPresent());
        });
    }

    @Test
    void findAll() {
        assertDoesNotThrow(()-> {
            userService.findAll();
            verify(userRepository,times(1)).findAll();
        });
    }

    @Test
    void saveAs() {
        assertDoesNotThrow(()->{
            userService.saveAs(any(),any());
            verify(userRepository,times(1)).saveAs(any(),any());
        });
    }

    @Test
    void loadFrom() {
        assertDoesNotThrow(()->{
            userService.loadFrom(any(),any());
            verify(userRepository,times(1)).loadFrom(any(),any());
        });
    }
}