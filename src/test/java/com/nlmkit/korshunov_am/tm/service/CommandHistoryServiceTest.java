package com.nlmkit.korshunov_am.tm.service;

import com.nlmkit.korshunov_am.tm.repository.CommandHistoryRepository;
import org.junit.jupiter.api.BeforeAll;
import org.mockito.*;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CommandHistoryServiceTest {
    static CommandHistoryRepository commandHistoryRepository;
    static CommandHistoryService commandHistoryService;

    @BeforeAll
    public static void doBeforeAll() {
        commandHistoryRepository = Mockito.mock(CommandHistoryRepository.class);
        commandHistoryService = new CommandHistoryService(commandHistoryRepository);
    }


    @Test
    public void getInstance() {
        CommandHistoryService commandHistoryService = CommandHistoryService.getInstance();
        assertEquals(commandHistoryService,CommandHistoryService.getInstance());
    }

    @Test
    public void addCommandToHistory() {
        Mockito.doAnswer(inv -> {
            Object arg0 = inv.getArgument(0);
            assertEquals("e1", arg0.toString());
            return null;
        }).when(commandHistoryRepository).AddCommandToHistory("e1");
        commandHistoryService.addCommandToHistory("e1");
    }

    @Test
    public void addCommandParameterToLastCommand() {
        Mockito.doAnswer(inv -> {
            Object arg0 = inv.getArgument(0);
            Object arg1 = inv.getArgument(1);
            assertEquals("e3", arg0.toString());
            assertEquals("e4", arg1.toString());
            return null;
        }).when(commandHistoryRepository).AddCommandParameterToLastCommand("e3","e4");
        commandHistoryService.AddCommandParameterToLastCommand("e3","e4");
    }

    @Test
    public void addCommandResultToLastCommand() {
        Mockito.doAnswer(inv -> {
            Object arg0 = inv.getArgument(0);
            assertEquals("e5", arg0.toString());
            return null;
        }).when(commandHistoryRepository).AddCommandResultToLastCommand("e5");
        commandHistoryService.AddCommandResultToLastCommand("e5");
    }

    @Test
    public void findAll() {
        commandHistoryService.findAll();
        Mockito.verify(commandHistoryRepository,Mockito.times(1)).findAll();
    }
}