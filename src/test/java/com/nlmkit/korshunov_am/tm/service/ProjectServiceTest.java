package com.nlmkit.korshunov_am.tm.service;

import com.nlmkit.korshunov_am.tm.entity.Project;
import com.nlmkit.korshunov_am.tm.exceptions.ProjectNotFoundException;
import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;
import com.nlmkit.korshunov_am.tm.repository.ProjectRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

class ProjectServiceTest {
    static ProjectRepository projectRepository;
    static ProjectService projectService;

    @BeforeAll
    public static void doBeforeAll() {
        projectRepository = Mockito.mock(ProjectRepository.class);
        projectService = new ProjectService(projectRepository);
    }

    @Test
    void getInstance() {
        ProjectService projectService = ProjectService.getInstance();
        assertEquals(projectService,ProjectService.getInstance());
    }

    @Test
    void create() {
        Mockito.doAnswer(inv -> {
            String arg0 = inv.getArgument(0);
            String arg1 = inv.getArgument(1);
            Long arg2 = inv.getArgument(2);
            assertEquals("name", arg0);
            assertEquals("description", arg1);
            assertEquals(1L, arg2.longValue());
            return null;
        }).when(projectRepository).create("name","description",1L);
        assertDoesNotThrow(()->{projectService.create("name","description",1L);});
        assertThrows(WrongArgumentException.class,()-> projectService.create(null,"description",1L));
        assertThrows(WrongArgumentException.class,()-> projectService.create("","description",1L));
        assertThrows(WrongArgumentException.class,()-> projectService.create("name",null,1L));
        assertThrows(WrongArgumentException.class,()-> projectService.create("name","",1L));
        assertThrows(WrongArgumentException.class,()-> projectService.create("name","description",null));
    }

    @Test
    void update() {
        Mockito.doAnswer(inv -> {
            Long arg0 = inv.getArgument(0);
            String arg1 = inv.getArgument(1);
            String arg2 = inv.getArgument(2);
            Long arg3 = inv.getArgument(3);
            assertEquals(2L, arg0.longValue());
            assertEquals("name", arg1);
            assertEquals("description", arg2);
            assertEquals(1L, arg3.longValue());
            return null;
        }).when(projectRepository).update(2L,"name","description",1L);
        assertDoesNotThrow(()->{projectService.update(2L,"name","description",1L);});
        assertThrows(WrongArgumentException.class,()-> projectService.update(2L,null,"description",1L));
        assertThrows(WrongArgumentException.class,()-> projectService.update(2L,"","description",1L));
        assertThrows(WrongArgumentException.class,()-> projectService.update(2L,"name",null,1L));
        assertThrows(WrongArgumentException.class,()-> projectService.update(2L,"name","",1L));
        assertThrows(WrongArgumentException.class,()-> projectService.update(2L,"name","description",null));
    }

    @Test
    void clear() {
        AtomicReference<Boolean> clearEnter = new AtomicReference<>();
        clearEnter.set(false);
        Mockito.doAnswer(inv -> {
            clearEnter.set(true);
            return null;
        }).when(projectRepository).clear();
        projectService.clear();
        assertTrue(clearEnter.get());
    }

    @Test
    void testClear() {
        Mockito.doAnswer(inv -> {
            Long arg0 = inv.getArgument(0);
            assertEquals(2L, arg0.longValue());
            return null;
        }).when(projectRepository).clear(2L);
        assertDoesNotThrow(()-> projectService.clear(2L));
        assertThrows(WrongArgumentException.class,()->projectService.clear(null));
    }

    @Test
    void findByIndex() {
        Mockito.doAnswer(inv -> {
            Integer arg0 = inv.getArgument(0);
            assertEquals(0, arg0.intValue());
            return null;
        }).when(projectRepository).findByIndex(0);
        assertDoesNotThrow(()->{projectService.findByIndex(0,false);});
        assertThrows(WrongArgumentException.class,()->projectService.findByIndex(null,false));
        assertThrows(WrongArgumentException.class,()->projectService.findByIndex(-1,false));
        assertThrows(ProjectNotFoundException.class,()->projectService.findByIndex(0,true));
    }

    @Test
    void testFindByIndex() {
        Mockito.doAnswer(inv -> {
            Integer arg0 = inv.getArgument(0);
            Long arg1 = inv.getArgument(1);
            assertEquals(0, arg0.intValue());
            assertEquals(0, arg1.longValue());
            return null;
        }).when(projectRepository).findByIndex(0,0L);
        assertDoesNotThrow(()->{projectService.findByIndex(0,0L,false);});
        assertThrows(WrongArgumentException.class,()->projectService.findByIndex(null,0L,false));
        assertThrows(WrongArgumentException.class,()->projectService.findByIndex(-1,0L,false));
        assertThrows(ProjectNotFoundException.class,()->projectService.findByIndex(0,0L,true));
    }

    @Test
    void findByName() {
        Mockito.doAnswer(inv -> {
            String arg0 = inv.getArgument(0);
            assertEquals("name", arg0);
            return null;
        }).when(projectRepository).findByName("name");
        assertDoesNotThrow(()-> assertNull(projectService.findByName("name",false)));
        assertThrows(WrongArgumentException.class,()->projectService.findByName(null,false));
        assertThrows(WrongArgumentException.class,()->projectService.findByName("",false));
        assertThrows(ProjectNotFoundException.class,()->projectService.findByName("name",true));
    }

    @Test
    void testFindByName() {
        Mockito.doAnswer(inv -> {
            String arg0 = inv.getArgument(0);
            Long arg1 = inv.getArgument(1);
            assertEquals("name", arg0);
            assertEquals(0L, arg1.longValue());
            return null;
        }).when(projectRepository).findByName("name",0L);
        assertDoesNotThrow(()-> assertNull(projectService.findByName("name",0L,false)));
        assertThrows(WrongArgumentException.class,()->projectService.findByName(null,0L,false));
        assertThrows(WrongArgumentException.class,()->projectService.findByName("",0L,false));
        assertThrows(ProjectNotFoundException.class,()->projectService.findByName("name",0L,true));
    }

    @Test
    void findById() {
        Mockito.doAnswer(inv -> {
            Long arg0 = inv.getArgument(0);
            assertEquals(0L, arg0.longValue());
            return null;
        }).when(projectRepository).findById(0L);
        assertDoesNotThrow(()->{projectService.findById(0L,false);});
        assertThrows(WrongArgumentException.class,()->projectService.findById(null,false));
        assertThrows(ProjectNotFoundException.class,()->projectService.findById(0L,true));
    }

    @Test
    void testFindById() {
        Mockito.doAnswer(inv -> {
            Long arg0 = inv.getArgument(0);
            Long arg1 = inv.getArgument(1);
            assertEquals(0L, arg0.longValue());
            assertEquals(0L, arg1.longValue());
            return null;
        }).when(projectRepository).findById(0L,0L);
        assertDoesNotThrow(()->{projectService.findById(0L,0L,false);});
        assertThrows(WrongArgumentException.class,()->projectService.findById(null,0L,false));
        assertThrows(ProjectNotFoundException.class,()->projectService.findById(0L,0L,true));
    }

    @Test
    void removeByIndex() {
        Mockito.doAnswer(inv -> {
            Integer arg0 = inv.getArgument(0);
            assertEquals(0, arg0.intValue());
            return null;
        }).when(projectRepository).removeByIndex(0);
        Mockito.doReturn(new Project()).when(projectRepository).removeByIndex(1);
        assertDoesNotThrow(()-> assertNotNull(projectService.removeByIndex(1)));
        assertThrows(WrongArgumentException.class,()->projectService.removeByIndex(null));
        assertThrows(WrongArgumentException.class,()->projectService.removeByIndex(-1));
        assertThrows(ProjectNotFoundException.class,()->projectService.removeByIndex(0));
    }

    @Test
    void testRemoveByIndex() {
        Mockito.doAnswer(inv -> {
            Integer arg0 = inv.getArgument(0);
            Long arg1 = inv.getArgument(1);
            assertEquals(0, arg0.intValue());
            assertEquals(0L, arg1.longValue());
            return null;
        }).when(projectRepository).removeByIndex(0,0L);
        Mockito.doReturn(new Project()).when(projectRepository).removeByIndex(1,0L);
        assertDoesNotThrow(()-> assertNotNull(projectService.removeByIndex(1,0L)));
        assertThrows(WrongArgumentException.class,()->projectService.removeByIndex(null,0L));
        assertThrows(WrongArgumentException.class,()->projectService.removeByIndex(-1,0L));
        assertThrows(WrongArgumentException.class,()->projectService.removeByIndex(0,null));
        assertThrows(ProjectNotFoundException.class,()->projectService.removeByIndex(0,0L));
    }

    @Test
    void removeById() {
        Mockito.doAnswer(inv -> {
            Long arg0 = inv.getArgument(0);
            assertEquals(0L, arg0.longValue());
            return null;
        }).when(projectRepository).removeById(0L);
        Mockito.doReturn(new Project()).when(projectRepository).removeById(1L);
        assertDoesNotThrow(()->{projectService.removeById(1L);});
        assertThrows(WrongArgumentException.class,()->projectService.removeById(null));
        assertThrows(ProjectNotFoundException.class,()->projectService.removeById(0L));
    }

    @Test
    void testRemoveById() {
        Mockito.doAnswer(inv -> {
            Long arg0 = inv.getArgument(0);
            Long arg1 = inv.getArgument(1);
            assertEquals(0L, arg0.longValue());
            assertEquals(0L, arg1.longValue());
            return null;
        }).when(projectRepository).removeById(0L);
        Mockito.doReturn(new Project()).when(projectRepository).removeById(1L,0L);
        assertDoesNotThrow(()->{projectService.removeById(1L,0L);});
        assertThrows(WrongArgumentException.class,()->projectService.removeById(null,0L));
        assertThrows(WrongArgumentException.class,()->projectService.removeById(0L,null));
        assertThrows(ProjectNotFoundException.class,()->projectService.removeById(0L,0L));
    }

    @Test
    void removeByName() {
        Mockito.doAnswer(inv -> {
            String arg0 = inv.getArgument(0);
            assertEquals("name", arg0);
            return null;
        }).when(projectRepository).removeByName("name");
        Mockito.doReturn(new Project()).when(projectRepository).removeByName("name1");
        assertDoesNotThrow(()->{projectService.removeByName("name1");});
        assertThrows(WrongArgumentException.class,()->projectService.removeByName(null));
        assertThrows(WrongArgumentException.class,()->projectService.removeByName(""));
        assertThrows(ProjectNotFoundException.class,()->projectService.removeByName("name"));
    }

    @Test
    void testRemoveByName() {
        Mockito.doAnswer(inv -> {
            String arg0 = inv.getArgument(0);
            Long arg1 = inv.getArgument(1);
            assertEquals("name", arg0);
            assertEquals(0L, arg1.longValue());
            return null;
        }).when(projectRepository).removeByName("name",0L);
        Mockito.doReturn(new Project()).when(projectRepository).removeByName("name1",0L);
        assertDoesNotThrow(()-> assertNotNull(projectService.removeByName("name1",0L)));
        assertThrows(WrongArgumentException.class,()->projectService.removeByName(null,0L));
        assertThrows(WrongArgumentException.class,()->projectService.removeByName("",0L));
        assertThrows(WrongArgumentException.class,()->projectService.removeByName("name",null));
        assertThrows(ProjectNotFoundException.class,()->projectService.removeByName("name",0L));
    }

    @Test
    void findAll() {
        AtomicReference<Boolean> isEntered = new AtomicReference<>();
        isEntered.set(false);
        Mockito.doAnswer(inv -> {
            isEntered.set(true);
            return null;
        }).when(projectRepository).findAll();
        projectService.findAll();
        assertTrue(isEntered.get());
    }

    @Test
    void testFindAll() {
        AtomicReference<Boolean> isEntered = new AtomicReference<>();
        isEntered.set(false);
        Mockito.doAnswer(inv -> {
            isEntered.set(true);
            return null;
        }).when(projectRepository).findAll(0L);
        assertDoesNotThrow(()->{projectService.findAll(0L);});
        assertThrows(WrongArgumentException.class,()->projectService.findAll(null));
        assertTrue(isEntered.get());
    }

    @Test
    void saveAs() {
        AtomicReference<Boolean> isEntered = new AtomicReference<>();
        isEntered.set(false);
        try {
            Mockito.doAnswer(inv -> {
                isEntered.set(true);
                return null;
            }).when(projectRepository).saveAs(any(),any());
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertDoesNotThrow(()-> projectService.saveAs(any(),any()));
        assertTrue(isEntered.get());
    }

    @Test
    void loadFrom() {
        AtomicReference<Boolean> isEntered = new AtomicReference<>();
        isEntered.set(false);
        try {
            Mockito.doAnswer(inv -> {
                isEntered.set(true);
                return null;
            }).when(projectRepository).loadFrom(any(),any());
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertDoesNotThrow(()-> projectService.loadFrom(any(),any()));
        assertTrue(isEntered.get());

    }
}