package com.nlmkit.korshunov_am.tm.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.nlmkit.korshunov_am.tm.enumerated.Role;
import com.nlmkit.korshunov_am.tm.entity.User;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

public class UserRepositoryTest {
    static UserRepository userRepository = UserRepository.getInstance();

    @BeforeAll
    public static void setUp() throws Exception {


        userRepository.create("testCreate1", Role.USER,"fn","sn","mn","ph");
        userRepository.create("testCreate2", Role.USER,"fn","sn","mn","ph");

        ByteArrayOutputStream byteArrayOutputStream1 = new ByteArrayOutputStream();

        userRepository.saveAs(new ObjectMapper(),byteArrayOutputStream1);
        userRepository.clear();
        ByteArrayInputStream byteArrayInputStream1 = new ByteArrayInputStream(byteArrayOutputStream1.toByteArray());
        userRepository.loadFrom(new ObjectMapper(),byteArrayInputStream1);

        ByteArrayOutputStream byteArrayOutputStream2 = new ByteArrayOutputStream();
        userRepository.saveAs(new XmlMapper(),byteArrayOutputStream2);
        userRepository.clear();
        ByteArrayInputStream byteArrayInputStream2 = new ByteArrayInputStream(byteArrayOutputStream2.toByteArray());
        userRepository.loadFrom(new XmlMapper(),byteArrayInputStream2);
        userRepository.clear();


    }

    @Test
    public void getInstance() {
        assertEquals(userRepository,UserRepository.getInstance());
    }

    @Test
    public void create() {
        User user =  userRepository.create("create");
        assertEquals(userRepository.findById(user.getId()).get(),user);
    }

    @Test
    public void testCreate() {
        User user =  userRepository.create("testCreate", Role.USER,"fn","sn","mn","ph");
        assertEquals(userRepository.findById(user.getId()).get(),user);
    }

    @Test
    public void update() {
        assertDoesNotThrow(()->{
            userRepository.update(null,"update1",Role.USER,"fn","sn","mn","ph");
        });
        User user =  userRepository.create("update");
        assertDoesNotThrow(()->{
            userRepository.update(user.getId(),"update1",Role.USER,"fn","sn","mn","ph");
        });
        assertEquals(userRepository.findByLogin("update1").get(),user);
    }

    @Test
    public void updateData() {
        User user =  userRepository.create("updateData");
        assertDoesNotThrow(()->{
            userRepository.updateData(null,"updateData1",Role.USER,"fn","sn","mn");
        });
        assertDoesNotThrow(()->{
            userRepository.updateData(user.getId(),"updateData1",Role.USER,"fn","sn","mn");
        });
        assertEquals(userRepository.findByLogin("updateData1").get(),user);
    }

    @Test
    public void updatePasswordHash() {
        User user =  userRepository.create("updatePasswordHash");
        user.setPasswordHash("ph");
        assertDoesNotThrow(()->{
            userRepository.updatePasswordHash(null,"ph1");
        });
        assertDoesNotThrow(()->{
            userRepository.updatePasswordHash(user.getId(),"ph1");
        });
        assertEquals(user.getPasswordHash(),"ph1");
    }

    @Test
    public void clear() {
        userRepository.create("clear");
        userRepository.clear();
        assertEquals(userRepository.size(),0);
    }

    @Test
    public void findById() {
        User user =  userRepository.create("findById");
        assertEquals(user,userRepository.findById(user.getId()).get());
    }

    @Test
    public void findByIdNull() {
        userRepository.create("findByIdNull");
        assertFalse(userRepository.findById(1L).isPresent());
    }

    @Test
    public void findByIndex() {
        User user =  userRepository.create("000000findByIndex");
        assertEquals(user,userRepository.findByIndex(0).get());
    }

    @Test
    public void findByLogin() {
        User user =  userRepository.create("findByLogin");
        assertEquals(user,userRepository.findByLogin("findByLogin").get());
    }

    @Test
    public void findByLoginNull() {
        userRepository.create("findByLoginNull");
        assertFalse(userRepository.findByLogin("findByLoginNull1").isPresent());
    }

    @Test
    public void removeById() {
        assertDoesNotThrow(()->{
            userRepository.removeById(null);
        });
        User user =  userRepository.create("removeById");
        assertDoesNotThrow(()->{
            userRepository.removeById(user.getId());
        });
        assertFalse(userRepository.findById(user.getId()).isPresent());
    }

    @Test
    public void findAll() {
        userRepository.create("findAll");
        assertNotEquals(userRepository.findAll().size(),0);
    }

    @Test
    public void size() {
        userRepository.create("findAll");
        assertNotEquals(userRepository.size(),0);
    }
}